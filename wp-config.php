<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_emilie' );

/** MySQL database username */
define( 'DB_USER', 'emilie38' );

/** MySQL database password */
define( 'DB_PASSWORD', 'emilie' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@m=^*%!lV],Q/Koz]+Wkw<-)aE%nQUdTGiMnJ|d%/{`w.+,t4<CN^/{%]!}7/)9L' );
define( 'SECURE_AUTH_KEY',  '%0Ucnm7FQ(<YnmbC8lRs+R`H[g:vNI(>T%UZW_j&q[rP IK&^}shU(-,$~9~[=/H' );
define( 'LOGGED_IN_KEY',    'fXz~{6e<Vt.t:q*9nz1Gby~WR70Q9bCRpw^7;0Sxw?Mz8Cc,}wqdnlsRH&Jl@1_`' );
define( 'NONCE_KEY',        'U4RF/oTEh]f@4wDJ?$1Cc`]X.Rhi!sivr1W3*q%Wz>~VJ]T:k8I.h|Y)|Ze:Br5U' );
define( 'AUTH_SALT',        'LgW1g0s7Hf&k.x/2pW@`M/J>no_ @lI[b#!>[&W(l]8y}Na|~-5aLlDg%c(r[xAc' );
define( 'SECURE_AUTH_SALT', '|6>xR/WWda9DB?fY${@Pz])kJ2%IVb?So(3}Ew?M+5H]I]0B+PPq@*3;tgFcZFz_' );
define( 'LOGGED_IN_SALT',   ' BPD!o42*|F)@.G4ti=EIW23-ore=e(A)A]O?M|m)$y{nyzm>d&uumaLB4XR<L#!' );
define( 'NONCE_SALT',       'nRQ2e VoEUHm Kc9;!MV}$HiES*y~~n5$qDCWCzf/7_;o;x%@#B+%I!O*?-bNCo%' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
